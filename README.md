# Html y css básico
En este repositorio se encuentran los recursos relacionados y tareas de la asignatura.
## Descripción
En este curso se abordan los conceptos básicos y las buenas prácticas para 
construir contenido web bien estructurado y fácil de mantener al conservar 
las etiquetas y reglas de estilo en orden, pero sobre todo pensado para el 
trabajo colaborativo.
## Autor
* González Sánchez Rodrigo
### Contacto
rodrigogonsan1997@gmail.com

